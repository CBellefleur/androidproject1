package com.example.christian.project1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TrialsActivity extends AppCompatActivity {
    ArrayList<Post> postList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trials);

        postList = new ArrayList<Post>();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("post");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                postList = new ArrayList<Post>();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    Post newPost = postSnapshot.getValue(Post.class);
                    postList.add(newPost);
                    PostsAdapter postsAdapter = new PostsAdapter(getBaseContext(), postList);
                    ListView listView = (ListView) findViewById(R.id.trialsList);
                    listView.setAdapter(postsAdapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        PostsAdapter postsAdapter = new PostsAdapter(this, postList);
        ListView listView = (ListView) findViewById(R.id.trialsList);
        listView.setAdapter(postsAdapter);
        registerForContextMenu(listView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.run_menu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        ListView listView = (ListView) findViewById(R.id.trialsList);
        Post post = (Post)listView.getItemAtPosition(info.position);
        Log.i("CONTEXT", post.toString());

        Intent i = new Intent(TrialsActivity.this, MapsActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putString("coords", post.coords);
        i.putExtras(mBundle);
        startActivity(i);

        return true;
    }



}
