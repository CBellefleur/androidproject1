package com.example.christian.project1;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

public class RunProvider extends ContentProvider {
    public final static String DBNAME = "RunProvider";
    public final static String TABLE_NAMESTABLE = "Records";
    public final static String ID = "_id";
    public final static String TIME = "Time";
    public final static String SADDRESS = "SAddress";
    public final static String EADDRESS = "EAddress";
    public final static String DISTANCE = "Distance";
    public final static String COORDS = "Coordinates";
    public final static String SQL_Create_Main =
            "CREATE TABLE " + TABLE_NAMESTABLE +  " ( " +
                    ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    TIME + " TEXT, " +
                    SADDRESS + " TEXT, " +
                    EADDRESS + " TEXT, " +
                    DISTANCE + " REAL, " +
                    COORDS + " TEXT )";
    public static final Uri CONTENT_URI =
            Uri.parse("content://com.example.christian.project1.provider");

    MainDatabaseHelper mOpenHelper;

    protected static final class MainDatabaseHelper extends SQLiteOpenHelper
    {
        MainDatabaseHelper(Context context)
        {
            super(context, DBNAME, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            db.execSQL(SQL_Create_Main);
        }
        @Override
        public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {}
    }

    public RunProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return mOpenHelper.getWritableDatabase()
                .delete(TABLE_NAMESTABLE, selection, selectionArgs);
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long id = mOpenHelper.getWritableDatabase().insert(TABLE_NAMESTABLE, null, values);
        return Uri.withAppendedPath(CONTENT_URI, "" + id);
    }


    @Override
    public boolean onCreate() {
        mOpenHelper = new MainDatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        return mOpenHelper.getReadableDatabase()
                .query(TABLE_NAMESTABLE, projection,selection,selectionArgs, null, null, sortOrder);
    }


    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
