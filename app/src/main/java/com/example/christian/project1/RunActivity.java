package com.example.christian.project1;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RunActivity extends Activity {
    private Location last;
    private Location start;
    private Location destination = null;
    private double distance;
    private LocationManager lm;
    private boolean timerStopped = true;
    private List<LatLng> locationPoints;
    final android.location.LocationListener ll = new android.location.LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Chronometer timer = (Chronometer) findViewById(R.id.chronometer);
            if (!timerStopped) {
                if (last != null) {
                    float l = location.distanceTo(last);
                    distance += l;
                    locationPoints.add(new LatLng(location.getLatitude(), location.getLongitude()));
                    TextView dtv = (TextView) findViewById(R.id.distText);
                    dtv.setText(new BigDecimal(distance).round(new MathContext(4)) + " m");

                    float p = location.distanceTo(destination);
                    TextView prox = (TextView) findViewById(R.id.proxText);
                    prox.setText(new BigDecimal(p).round(new MathContext(4)) + " m");
                    if (p <= 10.0f) {
                        timer.stop();
                        timerStopped = true;
                        Button s = (Button) findViewById(R.id.startButton);
                        s.setEnabled(false);
                        ContentValues values = new ContentValues();
                        insertRun();
                        Toast.makeText(getBaseContext(), "Run saved to database", Toast.LENGTH_LONG).show();
                    }
                } else {
                    float l = location.distanceTo(start);
                    distance += l;
                    locationPoints.add(new LatLng(location.getLatitude(), location.getLongitude()));
                    TextView dtv = (TextView) findViewById(R.id.distText);
                    dtv.setText(new BigDecimal(distance).round(new MathContext(4)) + " m");
                    if (location.distanceTo(destination) <= 10.0f) {
                        timer.stop();
                        timerStopped = true;
                        Button s = (Button) findViewById(R.id.startButton);
                        s.setEnabled(false);
                        insertRun();
                        Toast.makeText(getBaseContext(), "Run saved to database", Toast.LENGTH_LONG).show();
                    }
                }

                last = new Location(location);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run);
        lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationPoints = new ArrayList<LatLng>();
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1001L, 1.0f, ll);
        Bundle bundle = getIntent().getParcelableExtra("bundle");
        LatLng fromPosition = bundle.getParcelable("from_loc");
        LatLng toPosition = bundle.getParcelable("to_loc");
        locationPoints.add(fromPosition);
        last = null;
        start = new Location(LocationManager.GPS_PROVIDER);
        destination = new Location(LocationManager.GPS_PROVIDER);
        start.setLatitude(fromPosition.latitude);
        start.setLongitude(fromPosition.longitude);
        destination.setLatitude(toPosition.latitude);
        destination.setLongitude(toPosition.longitude);
        distance = 0.0;

        TextView distv = (TextView) findViewById(R.id.distText);
        distv.setText(distance + " m");

        Button start = (Button) findViewById(R.id.startButton);
        Chronometer timer = (Chronometer) findViewById(R.id.chronometer);
        timer.start();
        timerStopped = false;
        Button stop = (Button) findViewById(R.id.stopButton);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private String generateCoordStr() {
        String coordList = "";
        for (int i = 0; i < locationPoints.size(); i++)
        {
            coordList += locationPoints.get(i).latitude + "," +
                    locationPoints.get(i).longitude;
            if (i != locationPoints.size()-1)
                coordList += "|";
        }
        return coordList;
    }

    private void insertRun() {
        if (start != null && destination != null) {
            LatLng s = new LatLng(start.getLatitude(), start.getLongitude());
            LatLng d = new LatLng(destination.getLatitude(), destination.getLongitude());
            Chronometer timer = (Chronometer) findViewById(R.id.chronometer);

            Geocoder geocoder;
            List<Address> sAddresses = null;
            List<Address> dAddresses = null;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                sAddresses = geocoder.getFromLocation(s.latitude, s.longitude, 1);
                dAddresses = geocoder.getFromLocation(d.latitude, d.longitude, 1);

            } catch (Exception e) {
            }
            String saddress = sAddresses.get(0).getAddressLine(0);
            String daddress = dAddresses.get(0).getAddressLine(0);

            ContentValues contentValues = new ContentValues();
            contentValues.put(RunProvider.TIME, timer.getText().toString());
            contentValues.put(RunProvider.SADDRESS, saddress);
            contentValues.put(RunProvider.EADDRESS, daddress);
            contentValues.put(RunProvider.DISTANCE, new BigDecimal(distance)
                    .round(new MathContext(4)).floatValue());
            contentValues.put(RunProvider.COORDS, generateCoordStr());
            getContentResolver().insert(RunProvider.CONTENT_URI, contentValues);
        }
    }
}
