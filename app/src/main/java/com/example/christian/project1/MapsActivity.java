package com.example.christian.project1;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.Console;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback,
        OnMyLocationButtonClickListener,
        GoogleMap.OnMapLongClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback {

    private GoogleMap mMap;
    private LatLng myLoc;
    private LatLng toLoc;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;
    private List<LatLng> pRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String coords = bundle.getString("coords");
            pRoute = parseCoords(coords);
        }
    }

    private List<LatLng> parseCoords(String coords) {
        List<LatLng> l = new ArrayList<LatLng>();
        String[] pairs  = coords.split("\\|");
        for (String p: pairs) {
            String[] splitPair = p.split(",");
            l.add(new LatLng(Double.parseDouble(splitPair[0]), Double.parseDouble(splitPair[1])));
        }
        return l;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMapLongClickListener(this);
        enableMyLocation();

        if (pRoute != null) {
            PolylineOptions polylineOptions = new PolylineOptions();
            polylineOptions.addAll(pRoute);
            polylineOptions.width(5f).color(Color.BLUE);
            mMap.addPolyline(polylineOptions);
        }
        else {
            Log.i("PROUTE", "PROUTE IS EMPTY");
        }
    }

    public void onMapLongClick(LatLng point) {
        mMap.clear();
        if (pRoute != null) {
            PolylineOptions polylineOptions = new PolylineOptions();
            polylineOptions.addAll(pRoute);
            polylineOptions.width(5f).color(Color.BLUE);
            mMap.addPolyline(polylineOptions);
        }
        mMap.addMarker(new MarkerOptions().position(point));
        mMap.addPolyline(new PolylineOptions().add(myLoc, point).width(5f).color(Color.RED));
        toLoc = point;
        float[] results = new float[1];
        Location.distanceBetween(point.latitude, point.longitude,
                myLoc.latitude, myLoc.longitude,
                results);
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(point.latitude, point.longitude, 1);
            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        }
        catch(Exception e)
        {
            mMap.clear();
            return;
        }
        String address = addresses.get(0).getAddressLine(0);

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Start Run");
        alertDialog.setMessage("Do you want to choose this location?\n" + address
        + "\nDistance: " + new BigDecimal(results[0]).round(new MathContext(4)) + " meters");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(MapsActivity.this, RunActivity.class);
                        Bundle args = new Bundle();

                        args.putParcelable("from_loc", myLoc);
                        args.putParcelable("to_loc", toLoc);
                        i.putExtra("bundle", args);
                        finish();
                        startActivity(i);
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                       // mMap.clear();
                    }
                }
        );
        alertDialog.show();
    }

    public boolean onMyLocationButtonClick() {
        if ( ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_FINE_LOCATION )
                == PackageManager.PERMISSION_GRANTED ) {
            LocationManager locationManager = (LocationManager)
                    getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));
            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                myLoc = new LatLng(latitude, longitude);
                mMap.animateCamera(CameraUpdateFactory.newLatLng(myLoc));
            }
        }
        else {
            enableMyLocation();
        }
        return true;
    }
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
        else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
            LocationManager locationManager = (LocationManager)
                    getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));
            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                myLoc = new LatLng(latitude, longitude);
                float zoomLevel = (float) (18.5); //This goes up to 21
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, zoomLevel));
            }
        }

    }
    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            Toast.makeText(this, "Location access denied", Toast.LENGTH_SHORT);
            mPermissionDenied = false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enableMyLocation();
                } else {
                    mPermissionDenied = true;
                    Toast.makeText(this, "Location access denied", Toast.LENGTH_SHORT);
                }
                return;
            }
        }
    }
}
