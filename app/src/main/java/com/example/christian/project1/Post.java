package com.example.christian.project1;

public class Post {
    public String nickname;
    public double distance;
    public String time;
    public String message;
    public String coords;

    public Post(){}

    public Post(String nickname, double distance, String time, String message, String coords) {
        this.nickname = nickname;
        this.distance = distance;
        this.time = time;
        this.message = message;
        this.coords = coords;
    }

    @Override
    public String toString() {
        return "Name: " + nickname + "\nDistance: " + distance + "\nTime: " + time + "\nMessage: "
                + message;
    }
}
