package com.example.christian.project1;

import android.app.ListActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RecordsList extends ListActivity {
    Cursor mCursor;
    String[] mListColumns;
    int[] to;
    SimpleCursorAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);
        mListColumns = new String[]{RunProvider.ID, RunProvider.TIME,
                RunProvider.SADDRESS, RunProvider.EADDRESS, RunProvider.DISTANCE};

        mCursor = getContentResolver().query(RunProvider.CONTENT_URI, null, null, null, null);

        to = new int[]{R.id.id_entry, R.id.time_entry,
                R.id.saddr_entry, R.id.eaddr_entry, R.id.dist_entry};

        mAdapter = new SimpleCursorAdapter(this, R.layout.row, mCursor, mListColumns, to, 1);
        setListAdapter(mAdapter);
        registerForContextMenu(getListView());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.share_menu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        TextView runTextView =(TextView)getListView().getAdapter().getView(info.position, null,
                null).findViewById(R.id.id_entry);
        Log.i("CMENU", runTextView.getText().toString());
        switch(item.getItemId()) {
            case R.id.shareRun:
                String[] projection = null;
                String selection = RunProvider.ID + " = ?";
                String selectArgs[] = {runTextView.getText().toString()};
                Cursor mCursor = getContentResolver().query(RunProvider.CONTENT_URI, projection,
                        selection, selectArgs, null);
                if (mCursor == null) {
                    Log.e("CLIST", "Error reading DB");
                } else if (mCursor.getCount() < 1) {
                    Log.e("CLIST", "Not in DB");
                } else {
                    mCursor.moveToFirst();
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    final DatabaseReference myRef = database.getReference("post");
                    final String key = myRef.push().getKey();
                    Post newRun = new Post();
                    newRun.nickname = "Anonymous";
                    newRun.distance = mCursor.getFloat(mCursor.getColumnIndex(RunProvider.DISTANCE));
                    newRun.time = mCursor.getString(mCursor.getColumnIndex(RunProvider.TIME));
                    newRun.message = "Beat my time";
                    newRun.coords = mCursor.getString(mCursor.getColumnIndex(RunProvider.COORDS));
                    myRef.child(key).setValue(newRun);
                    Toast.makeText(this, "Run Uploaded", Toast.LENGTH_LONG).show();
                }
                break;
        }
        return true;
    }


    public void finish(View v)
    {
        finish();
    }
}