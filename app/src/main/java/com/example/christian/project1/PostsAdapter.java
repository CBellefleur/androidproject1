package com.example.christian.project1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;

public class PostsAdapter extends ArrayAdapter<Post> {
    public PostsAdapter(Context context, ArrayList<Post> posts) {
        super(context, 0, posts);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Post post = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_post, parent, false);
        }
        TextView name = (TextView) convertView.findViewById(R.id.postName);
        TextView distance = (TextView) convertView.findViewById(R.id.postDist);
        TextView time = (TextView) convertView.findViewById(R.id.postTime);
        TextView message = (TextView) convertView.findViewById(R.id.postMessage);
        name.setText(post.nickname);
        distance.setText(new BigDecimal(post.distance)
                .round(new MathContext(4)).floatValue() + " m");
        time.setText(post.time);
        message.setText(post.message);

        return convertView;
    }
}
